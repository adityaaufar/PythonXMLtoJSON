import xml.etree.ElementTree as ET
from lxml import etree
import xmltodict, json

tree = etree.parse('com.redhat.rhsa-all.xml')
root = tree.getroot()  # now get the root

# Remove namespace prefixes
for elem in root.getiterator():
    elem.tag = etree.QName(elem).localname

# Remove unused namespace declarations
etree.cleanup_namespaces(root)

list_include = ["definitions", "definition", "metadata", "title", "advisory", "reference", "severity",
                "affected_cpe_list", "cpe", "criteria", "criterion", "cve"]

new_root_string = etree.tostring(root, encoding='utf-8').decode()
new_root = ET.fromstring(new_root_string)

for element in new_root.iter():
    for child in list(element):
        if child.tag not in list_include:
            element.remove(child)

xmlstr = ET.tostring(new_root, encoding='utf8', method='xml').decode()
with open("filteredresult.xml", "w") as f:
    f.write(xmlstr)
    f.close()

# o = xmltodict.parse(xmlstr)
# jsonString = json.dumps(o, indent=4, sort_keys=True)
# jsonFile = open("data.json", "w")
# jsonFile.write(jsonString)
# jsonFile.close()




