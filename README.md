# PythonXMLtoJSON
Processing and cleaning XML File into JSON


# Origin
The small project I created as part of one of job interview/selection.


# The Purpose
The task is convert huge XML file and convert it to JSON.
The task is not as easy as convert it using library straight away, there are few conditions before coverting

# Current Version
The current version of the product so far it can filter out unnecessary tag or element before process it further to create JSON file (filteredresult.xml)

# The Problem
As the relationship between the tag or element from the XML file and the expected JSON attribute (the requirement from the job interview) is not one to one, I'm still stuck to generate the JSON file from scratch. The expected JSON file  format could have been generated from different XML format entirely (file expected_xml_format.xml for illustration)


